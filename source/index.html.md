---
title: GitLab Backstage Changelog
---

## Database

* [yorickpeterse](https://gitlab.com/yorickpeterse)
  * Set up the GitLab Backstage Changelog
  * Reviewed a million merge requests

## Platform

* [Billy](#)
  * Watched too many Reddit videos
