# frozen_string_literal: true

require 'uglifier'
require 'date'

Haml::TempleEngine.disable_option_validator!

page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false
page '/*.ico', layout: false

set :website_title, 'GitLab backstage changelog'
set :website_author, 'GitLab'
set :website_url, 'https://gitlab-org.gitlab.io/gitlab-backstage'
set :markdown_engine, :kramdown

set :markdown,
    fenced_code_blocks: true,
    parse_block_html: true,
    auto_ids: true,
    auto_id_prefix: 'header-',
    tables: true,
    input: 'GFM',
    hard_wrap: false,
    toc_levels: 1..3

set :haml, format: :html5

activate :syntax, line_numbers: false
activate :directory_indexes

configure :development do
  activate :livereload
end

configure :build do
  set :build_dir, 'public'
  set :base_url, '/backstage-changelog'

  activate :relative_assets

  activate :minify_css
  activate :minify_javascript, compressor: proc { Uglifier.new(harmony: true) }
  activate :asset_hash
end

helpers do
  def markdown(text)
    Tilt['markdown'].new(config.markdown) { text }.render(self)
  end

  def format_changelog_date(date)
    Date.strptime(date, '%Y-%m').strftime('%B %Y')
  end

  def changelog_entries_per_team(entries)
    entries.values.group_by { |entry| entry['team'] }.sort_by { |k, _| k }
  end

  def changelog_entries_per_author(entries)
    entries.group_by { |e| e['author'] }.sort_by { |k, _| k }
  end

  def sorted_changelog_entries
    data.changelog.sort { |a, b| b[0] <=> a[0] }
  end
end
